---
# Display name
name: Connor Kenyon

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Graduate Student

# Organizations/Affiliations
organizations:
- name: University of Massachusetts Dartmouth
  url: "http://umassd.edu/"

# Short bio (displayed in user profile at end of posts)
bio: CHANGEME

interests:
- High Performance Computing
- Computational Fluid Dynamics
- Numerical Relativity
- Data Science

education:
  courses:
  - course: PhD in Engineering and Applied Science
    institution: University of Massachusetts Dartmouth
    year: Expected 2023
  - course: Ms in Physics
    institution: University of Massachusetts Dartmouth
    year: Expected 2021
  - course: BS in Physics
    institution: University of Massachusetts Dartmouth
    year: 2018

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:ckenyon@umassd.edu".
- icon: github
  icon_pack: fab
  link: https://github.com/ConnorKenyon

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "ckenyon@umassd.edu"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

Connor Kenyon is a graduate student at the University of Massachusetts Dartmouth, studying Engineering and Applied science, with a focus in high performance computing in Physics. His research interests include GPU computing, numerical PDEs, and computational tools and education.

